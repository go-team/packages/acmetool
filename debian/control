Source: acmetool
Section: web
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders:
 Peter Colberg <peter@colberg.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-apache2,
 dh-golang,
Build-Depends-Arch:
 golang-any,
 golang-github-coreos-go-systemd-dev,
 golang-github-gofrs-uuid-dev,
 golang-github-hlandau-dexlogconfig-dev,
 golang-github-hlandau-goutils-dev,
 golang-github-hlandau-xlog-dev,
 golang-github-jmhodges-clock-dev,
 golang-github-mitchellh-go-wordwrap-dev,
 golang-golang-x-net-dev,
 golang-gopkg-alecthomas-kingpin.v2-dev,
 golang-gopkg-cheggaaa-pb.v1-dev,
 golang-gopkg-hlandau-acmeapi.v2-dev,
 golang-gopkg-hlandau-easyconfig.v1-dev,
 golang-gopkg-hlandau-service.v2-dev,
 golang-gopkg-hlandau-svcutils.v1-dev,
 golang-gopkg-square-go-jose.v1-dev,
 golang-gopkg-tylerb-graceful.v1-dev,
 golang-gopkg-yaml.v2-dev | golang-yaml.v2-dev,
 libcap-dev [linux-any],
Standards-Version: 4.6.2
Homepage: https://hlandau.github.io/acmetool
Vcs-Browser: https://salsa.debian.org/go-team/packages/acmetool
Vcs-Git: https://salsa.debian.org/go-team/packages/acmetool.git
XS-Go-Import-Path: github.com/hlandau/acmetool
Testsuite: autopkgtest-pkg-go
Rules-Requires-Root: no

Package: acmetool
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 dialog,
Built-Using:
 ${misc:Built-Using},
Description: automatic certificate acquisition tool for Let's Encrypt
 acmetool is an easy-to-use command line tool for automatically
 acquiring TLS certificates from ACME (Automated Certificate Management
 Environment) servers such as Let's Encrypt, designed to flexibly
 integrate into your webserver setup to enable automatic verification.
 .
 acmetool is designed to work like make: you specify what certificates
 you want, and acmetool obtains certificates as necessary to satisfy
 those requirements. If the requirements are already satisfied,
 acmetool doesn't do anything when invoked. Thus, acmetool is
 ideally suited for use on a cron job; it will do nothing until
 certificates are near expiry, and then obtain new ones.
 .
 acmetool is designed to minimise the use of state and be transparent
 in the state that it does use. All state, including certificates, is
 stored in a single directory, by default /var/lib/acme. The schema
 for this directory is simple, comprehensible and documented.
